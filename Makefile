get-titles:
	python utils/get_titles.py data/raw/imdb-titles.csv \
		--log-dir data/logs \
		--release 2000 \
		--rating 0 \
		--votes 1000 \
		--genres "" \
		--runtime 20.0 \
		--sleep 0.1
get-entities:
	python utils/get_entities.py data/raw \
		--input-glob "data/raw/imdb-titles.csv" \
		--max-workers 6 \
		--log-dir data/logs

