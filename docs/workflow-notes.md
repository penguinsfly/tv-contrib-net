# Notes on workflow

## Install `datalad`

To manage data, `datalad` + GIN G-Node are used. There are [many ways](https://handbook.datalad.org/en/latest/intro/installation.html) to install `datalad`.

This uses [`devbox`](https://www.jetpack.io/devbox) to install `datalad`, along with its dependencies (`git-annex`, `datalad-next` via `requirements-datalad.txt`

```bash
devbox shell # start devbox shell
devbox run install # only need to run once
exit # to exit debox shell 
```

## Git config

### Codeberg

First set Codeberg side, ignore git-annex on the Codeberg side to avoid data on Codeberg.

```bash
git config user.name <CODEBERG-USERNAME>
git config user.email <CODEBERG-EMAIL>

git remote add origin git@codeberg.org:penguinsfly/tv-contrib-net.git
git config remote.origin.annex-ignore true 
```

### GIN

Then activate `devbox` shell to use `datalad` and add GIN

```bash
devbox shell

datalad siblings add -d . \
  --name gin \
  --url git@gin.g-node.org:/penguinsfly/tv-contrib-net.git
```

## Saving data

```bash
datalad save \
  -m 'datalad: save raw' \
  --version-tag 'data.raw' \
  data
```

## Pushing

```bash
git push
datalad push --to gin
```

### Tags

To push tags, try either using:

```bash
git push --follow-tags
git push gin --follow-tags
```

Or specific tags if the above doesn't seem to work:

```bash
git push origin data.raw # specific remote just to be safe
git push gin data.raw
```

## Pulling

```bash
# clone from Codeberg
git clone https://codeberg.org/penguinsfly/tv-contrib-net

# add GIN remote
# assumed datalad is installed to some extent
datalad siblings add -d . \
  --name gin \
  --url https://gin.g-node.org/penguinsfly/tv-contrib-net
  # NOTE: there is NO ".git" at the end

git config remote.gin.annex-ignore false
datalad update

# Then get specific folders/files
datalad get data
```

