import os
import glob
import re
import time
import datetime
import argparse
from functools import partial

import pandas as pd
from tqdm.contrib.concurrent import process_map
import imdb

import logging
from rich.logging import RichHandler

IMDB_METADATA_KEYS = [
    'title', 
    'genres',  
    'rating', 
    'runtimes', 
    'votes',
    'year',
    'series years', 
    'number of seasons',
    'synopsis',
    'countries'
]

def get_IMDB_data(series_ID, ia=None, metadata_keys=['title', 'genres']):
    if ia is None:
        ia = imdb.Cinemagoer()
        
    series = ia.get_movie(series_ID)
    ia.update(series, 'full credits')

    metadata = dict(
        series_ID = series.getID(),
        type = 'series'
    )

    entities = []

    for k, v in series.items():
        if k in metadata_keys:
            metadata[k.replace(' ', '_')] = v
            continue

        if not isinstance(v, list):
            continue

        for obj in v:
            obj_type = None

            if isinstance(obj, imdb.Person.Person):
                obj_type = 'person'

            if isinstance(obj, imdb.Company.Company):
                obj_type = 'company'

            if obj_type is None:
                continue

            entities.append(dict(
                series_ID = metadata['series_ID'],
                entity_ID = obj.getID(),
                type = obj_type,
                name = obj.get('name', None),
                category = k
            ))

    df_entities = (
        pd.DataFrame(entities)
        .drop_duplicates(['series_ID', 'entity_ID', 'type']) # ignore multiple names of same entity ID
        .reset_index(drop=True)
    )
    
    return dict(
        series_ID = series_ID, 
        metadata = metadata, 
        entities = df_entities,
        success = True
    )

def get_IMDB_data_with_exception(series_ID, *args, **kwargs):
    try:
        data = get_IMDB_data(series_ID, *args, **kwargs)
    except:
        print(f'ERROR [{series_ID}]')
        data =  dict(
            series_ID = series_ID,
            success = False
        )
        
    return data

def gather_series_IDs_from_files(title_files, comment='#'):
    df_titles = []
    for file in title_files:
        df_titles.append(
            pd.read_csv(file, comment=comment)
            .assign(source=os.path.basename(file))
            .astype({'ID': str})
        )

    df_titles = pd.concat(df_titles, ignore_index=True)
    series_IDs = list(df_titles['ID'].unique())
    return series_IDs

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog='Gather IMDB entities and metadata from series IDs',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    
    parser.add_argument('output', default='data', help='Directory for output files')
    parser.add_argument('--input-glob', type=str, default='titles-*.csv', 
                        help='Glob patterns for finding input title list files')
    parser.add_argument('-j', '--max-workers', type=int, default=2, help='Max number of workers for parallel')
    parser.add_argument('--log-dir', default='logs', help='Directory of log file')
    
    
    args = parser.parse_args()
    OUTPUT_PATH = args.output
    TITLE_FILE_GLOB = args.input_glob
    MAX_WORKERS = args.max_workers
    LOG_PATH = args.log_dir
    
    # logging for general report (not for actual error during multiproc)
    log = logging.getLogger("rich")
    log_file = os.path.join(
        LOG_PATH, 
        datetime.datetime.now().strftime('get-entities-data_%Y%m%d_%H%M%S.log')
    )
    logging.basicConfig(
        format='%(message)s',
        datefmt="[%Y-%m-%d %H:%M:%S]", 
        handlers=[
            RichHandler(),
            logging.FileHandler(log_file)
        ],
        level=logging.INFO,
    )
    
    # gather series IDs from input title list files
    title_files = glob.glob(TITLE_FILE_GLOB)
    inp_file_str = "\n\t- ".join([""] + title_files)
    log.info(f'Reading from title files: {inp_file_str} ...')
    
    series_IDs = gather_series_IDs_from_files(title_files)
    num_series = len(series_IDs)
    log.info(f'Found {num_series} unique series IDs')
    
    # start imdb scrape
    log.info('Starting to scrape IMDB with Cinemagoer ...')
    
    data = process_map(
        partial(get_IMDB_data_with_exception, metadata_keys=IMDB_METADATA_KEYS),
        series_IDs,
        max_workers=MAX_WORKERS,
        chunksize=MAX_WORKERS*2
    )
    
    log.info('Done with IMDB scraping!')
    
    # report scrape stat
    success_data = [d for d in data if d['success']]
    failed_IDs = [d['series_ID'] for d in data if not d['success']]
    num_failed = len(failed_IDs)
    if num_failed == 0:
        log.info('No failed attempts during scraping')
    else:
        sep = '-' * 50
        log.warning(
            f'There were {num_failed}/{num_series} failed attempts during scraping! See below:\n'
            f'{sep}\n'
            f'{failed_IDs}' 
            f'{sep}\n' 
        )
        
    # save data
    meta_file = os.path.join(OUTPUT_PATH, 'imdb-metadata.feather')
    data_file = os.path.join(OUTPUT_PATH, 'imdb-entities.feather')
    
    meta_df = pd.DataFrame([d['metadata'] for d in success_data])
    data_df = pd.concat([d['entities'] for d in success_data], ignore_index=True)

    meta_df.to_feather(meta_file, compression='zstd')
    data_df.to_feather(data_file, compression='zstd')
    
    log.info(f'Metadata of series saved in "{meta_file}"')
    log.info(f'Entities data of series saved in "{data_file}"')
    log.info('DONE!')
