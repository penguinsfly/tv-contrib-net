import os
import re
import time
import datetime
import argparse

import pandas as pd
import csv

import requests
from bs4 import BeautifulSoup

import logging
from rich.logging import RichHandler
from rich.progress import Progress

IMDB_URL = 'https://www.imdb.com'
TITLE_REGEX = re.compile('/title/tt(\w+)/')

def request(url):
    r = requests.get(url)
    assert r.status_code == 200
    s = BeautifulSoup(r.content, 'lxml')
    return s

def follow_next(soup):
    nexts = soup.find_all(class_="lister-page-next next-page")
    if len(nexts) == 0:
        return None
    next_url = IMDB_URL + nexts[0].get('href')
    return next_url

def get_desc(soup):
    desc = s.find_all(class_="desc")
    if len(desc) == 0:
        return
    desc = (
        desc[0]  
        .get_text()
        .strip()
        .split('\n')
    )
    return desc[0]
    
def get_titles(soup):
    soup_title_list = soup.find_all(class_="lister-item-header")
    if len(soup_title_list) == 0:
        return None
    titles = []
    for st in soup_title_list:
        a = st.find_all('a')
        if len(a) == 0:
            continue
        link = a[0].get('href')
        title = a[0].get_text().strip()
        ID = TITLE_REGEX.search(link)
        if ID is None: 
            continue
        ID = ID.groups()[0]
        
        titles.append(dict(
            name=title,
            ID=ID
        ))
    return titles


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        prog='Get IMDB titles',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    
    parser.add_argument('output', default='data', help='File path for output')
    parser.add_argument('--log-dir', default='logs', help='Directory of log file')
    parser.add_argument('--sleep', type=float, default=1, help='Time to sleep between query pages')
    parser.add_argument('--type', type=str, default='tv_series', help='Title type from IMDB')
    parser.add_argument('--release', type=int, default=2023, help='Minimum release year')
    parser.add_argument('--votes', type=int, default=20000, help='Minimum number of votes')
    parser.add_argument('--rating', type=float, default=6.0, help='Minimum IMDB user ratings')
    parser.add_argument('--genres', type=str, default='comedy', help='Genres of to query')
    parser.add_argument('--runtime', type=float, default=20, help='Minimum run time')
    parser.add_argument('--sort', type=str, default='user_rating,asc', help='Sorting after query')
    
    args = parser.parse_args()
    OUTPUT_PATH = args.output
    LOG_PATH = args.log_dir
    SLEEP_TIME = args.sleep    

    if not os.path.exists(LOG_PATH):
        os.mkdir(LOG_PATH)
    
    QUERY_CONFIG = dict(
        title_type=args.type,
        release_date='%d,' %(args.release),
        num_votes='%d,' %(args.votes),
        user_rating='%.1f,' %(args.rating),
        genres=args.genres,
        runtime='%.1f,' %(args.runtime),
        sort=args.sort
    )
    
    # construct queries and seed URL
    QUERY = '&'.join(
        [k+'='+v for k, v in QUERY_CONFIG.items()]
    )
    
    seed_url = (
        f'{IMDB_URL}'
        '/search/title/'
        '?'
        f'{QUERY}'
    )

    # logging
    
    log = logging.getLogger("rich")
    log_file = os.path.join(
        LOG_PATH, 
        datetime.datetime.now().strftime('get-title-list_%Y%m%d_%H%M%S.log')
    )
    logging.basicConfig(
        format='%(message)s',
        datefmt="[%Y-%m-%d %H:%M:%S]", 
        handlers=[
            RichHandler(),
            logging.FileHandler(log_file)
        ],
        level="NOTSET",
    )


    # initialize
    titles = []
    url = seed_url
    has_next = True

    # scrape
    with Progress(transient=True) as progress:
        log.info('Starting scraping ...')
        task = None
        
        while has_next:
            log.info(f'REQUESTING: {url}')
            s = request(url)

            # description/stat
            desc = get_desc(s)
            if desc is not None:
                log.info(f'From description, found: {desc}')
            
            # create progress bar
            if task is None and desc is not None:
                total_titles = re.search('([\d,]+)\stitles', desc).groups()[0]
                total_titles = int(total_titles.replace(',',''))
                task = progress.add_task("[green]", total=total_titles)
                
            # titles
            ttls = get_titles(s)
            num_titles_found = len(ttls)
            if ttls is None:
                log.warning(f'From parsing title objects, could not find any link IDs')
            else:
                log.info(f'From parsing title objects, found {num_titles_found} IDs')
            titles.extend(ttls)

            # follow to next URL
            next_url = follow_next(s)
            has_next = next_url is not None 
            if not has_next:        
                log.info('No next URLs found. Exitting ...')
            else:
                log.info(f'Found next URL to follow: {next_url}')
            url = next_url

            # update progress bar
            if task is not None:
                progress.update(task, advance=num_titles_found)
                
            # sleep
            time.sleep(SLEEP_TIME)
            log.info(f'Sleep for {SLEEP_TIME} seconds')

    log.info(f'Finished scraping! Found in total {len(titles)} titles')

    # save to file
    with open(OUTPUT_PATH, 'w') as f:
        dt_record = datetime.datetime.now().strftime('# DATE: %Y-%m-%d %H:%M:%S\n')
        f.write(f'# SEED_URL: "{seed_url}"\n')
        f.write(f'# LOG_PATH: "{log_file}"\n')
        
        f.write('# QUERY:\n')
        for qk, qv in QUERY_CONFIG.items():
            f.write(f'#\t{qk}: "{qv}"\n')
        
        f.write(dt_record)
        
        pd.DataFrame(titles).to_csv(
            f, 
            index=False, 
            quoting=csv.QUOTE_ALL, 
            mode='a'
        )

    log.info(f'Output file is saved at "{OUTPUT_PATH}"')

    log.info('DONE!')
